# Node.js + Redis cache API template

Composes a Node.js app with a Redis cache

## Intro

So you want to create your own API, but what should you do next?

The most I learned in programming has been by example. Let me give
you a template where you can too.

Follow along and get your API with an integrated REDIS cache up and running.

## Feature overview

*   [x] **Custom endpoint examples** to see how to implement basic Use Cases
*   [x] **Caching with Redis** to boost performance dramatically
*   [x] **Hot reload** in development mode
*   [ ] **Tests**

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Configuration](#configuration)
    *   [Usage](#usage)
*   [Endpoints](#endpoints)
    *   [Basic](#basic)
    *   [Proxy](#proxy)
    *   [Redis](#redis)
*   [About](#about)
    * [Used Technologies](#used-technologies)
    * [inspect the Redis image](#inspect-the-redis-image)
    * [Testing](#testing)
    * [Logging](#logging)
*   [Contribute](#contribute)
*   [License](#license)
*   [Sources](#sources)
*   [Conclusion](#conclusion)

## What is this?

This project a feature rich template to get your Node.js API up and running. It is meant to be customized
to your needs and it&apos;s up to you to implement your Use Cases. Still this template gives you an insane
headstart.

## Why should I use this?

There are many Node.js templates out there and feel free to integrate them if you want to. But this template
is fully containarized and flexible so it serves your needs.

## Getting Started

So how do you get this template to work for your project? It is easier than you think.

### Requirements

* Have docker installed and setup
* Have docker-compose installed and setup
* That&apos;s it

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/nodejs-base
```

### Configuration

To configure the application create a `.env` file following the `.env.example` structure

### Usage

Run in development mode

```bash
./scripts/dev_run.sh
```

Run in production mode

```bash
./scripts/prod_run.sh
```

## Endpoints

Hit one of the available endpoints in your browser or using curl:

### Basic

**GET** `/` - Welcome message

**GET** `/random` - Get a random float 0.0 - 1.0

**GET** `/double/:number` - Doubles the entered number

**GET** `/multiply/:x/:y`

**GET** `/*xxx*/:number`

**POST** `/double`

**GET** `/middleware`

**GET** `/download`

**GET** `/chain`

### Proxy

**GET** `/proxy` - Proxied data

### Redis

**GET** `/redis/get` - Read stored message

**GET** `/redis/set/:message` - Store message


## About

Let&apos;s dive deeper into the template and it&apos;s amazing features.

### Used technologies

We are using the leading choice for creating an API. Node.js. With an integrated Redis cache.

### Inspect the Redis image

1) Enter the Redis image: `docker exec -it image_name bash`

2) Execute redis-cli commands (eg. `redis-cli flushall`)

### Testing

No tests are implemented yet.

### Logging

Standart Node.js middleware is used to create an access and error log in the API.

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources

[Redis][redis] - About Redis

[Node.js Docs][nodejs-doc] - Learn more about Node.js

[//]: # "Source definitions"
[redis]: https://redis.io/ "Redis"
[nodejs-doc]: https://nodejs.org/api/synopsis.html "Node.js Docs"

## Conclusion

To summarize..

We have an exhaustive Node.js template for our next API project. With many features and ready to be customized to fit our needs.
In our future projects we can use this template to get a great head start in creating a custom API.
