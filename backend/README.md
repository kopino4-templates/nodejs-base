# Node.js boilerplate project

This project container boilerplate for the following functionality:

- Redis caching
- Connection to MongoDB
- Enabled CORS
- Containerized using Docker
- Configurable by .env variables (dotenv-safe)
