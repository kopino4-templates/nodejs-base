import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv-safe';

import basic from './routes/basic';
import proxy from './routes/proxy';
import redis from './routes/redis';

const app = express();
const port = 3000;

// Load environment variables for .env
dotenv.config();

// Enabe CORS globally
app.use(cors());

// Serve the static files in "public" folder
// eg. localhost:3000/elephant.jpeg
app.use(express.static('public'));

// Use the Router modules
app.use('/', basic);
app.use('/proxy', proxy);
app.use('/redis', redis);

// Start the app
app.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`Example app listening at http://localhost:${port}`);
});
