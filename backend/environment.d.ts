declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DOCKERIZED: string;
      MONGODB_DB_NAME: string;
      MONGODB_URI: string;
    }
  }
}

export {};
