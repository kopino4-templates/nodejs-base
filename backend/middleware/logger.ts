// Configurable middleware for logging
// adds a param to req object
function configurableLog(options) {
  return function log(req, res, next): void {
    /* eslint-disable no-console */
    console.log(options.message, `[time: ${Date.now()}]`);
    req.requestTime = Date.now();
    next();
  };
}

module.exports = configurableLog;
