import exp, { Request, Response, NextFunction } from 'express';
const router = exp.Router();

// Root endpoint
router.get('/', (req: Request, res: Response) => {
  res.send('Welcome to the Kopino API!');
});

// Define a simple GET endpoint
router.get('/random', (req: Request, res: Response) => {
  const x = Math.random();
  res.send(`${x}`);
});

// Take parameter from path
router.get('/double/:number', (req: Request, res: Response) => {
  const number = parseInt(req.params.number);
  res.send(`${number * 2}`);
});

// Take two params from path, res: Response as JSON
router.get('/multiply/:x/:y', (req: Request, res: Response) => {
  const x = parseInt(req.params.x);
  const y = parseInt(req.params.y);
  res.json({ number: x * y });
});

// Use regex to define path
// ('{anything}xxx{anything}/{number}')
// eg. /ABCDxxx1234/8
router.get('/*xxx*/:number', (req: Request, res: Response) => {
  const number = parseInt(req.params.number);
  res.send(`${number * 2}`);
});

// Take param from POST request data
router.post('/double', (req: Request, res: Response) => {
  const number = parseInt(req.params.number);
  res.send(`${number * 2}`);
});

// Use middleware function
const log = (req: Request, res: Response, next: NextFunction): void => {
  //console.log('Date: ', Date.now());
  next();
};

const respond = (req: Request, res: Response): void => {
  res.send('A middleware function logged your timestamp to the console');
};

router.get('/middleware', [log, respond]);

// Download a file (send file in response)
router.get('/download', (req: Request, res: Response) => {
  res.download('public/elephant.jpeg', 'kopinoFile.jpeg');
});

// Chain handlers for one route path to reduce redundancy
router
  .route('/chain')
  .get((req: Request, res: Response) => {
    res.send('Chained handler for GET');
  })
  .put((req: Request, res: Response) => {
    res.send('Chained handler for PUT');
  });

export default router;
