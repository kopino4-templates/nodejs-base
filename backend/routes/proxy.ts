import exp, { Request, Response } from 'express';
import { fetchWithCache } from '../utils/cache';

const router = exp.Router();

router.get('/', (req: Request, res: Response) => {
  fetchWithCache('https://jsonplaceholder.typicode.com/posts?userId=1', 'posts', res);
});

export default router;
