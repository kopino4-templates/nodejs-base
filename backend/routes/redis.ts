import exp, { Request, Response } from 'express';
import { getMessage, setMessage } from '../utils/cache';

const router = exp.Router();

router.get('/set/:message', async (req: Request, res: Response) => {
  const message = req.params.message;
  await setMessage(message);
  res.send('Message has been stored');
});

router.get('/get', (req: Request, res: Response) => {
  getMessage((err, message) => {
    if (err) throw err;
    if (message) {
      res.send(message);
    } else {
      res.send('No message stored yet');
    }
  });
});

export default router;
