import express from 'express';
import axios from 'axios';
import * as redis from 'redis';
const host = process.env.DOCKERIZED === 'true' ? 'redis' : undefined;
const client = redis.createClient(6379, host);

const messageKey = 'messageKey';

export function fetchWithCache(url: string, key: string, res: express.Response): void {
  try {
    client.get(key, async (err, data) => {
      if (err) throw err;

      if (data) {
        //console.log(`Reading "${key}" from cache instead fetching "${url}"`);
        res.send(JSON.parse(data));
      } else {
        //console.log(`Fetching "${url}", no record found in cache for key "${key}"`);
        const dto = await axios.get(url);
        const newData = dto.data;
        client.set(key, JSON.stringify(newData));
        res.send(newData);
      }
    });
  } catch (err) {
    const msg = (err as Error).message;
    res.status(500).send({ message: msg });
  }
}

export async function setMessage(message: string): Promise<void> {
  await client.set(messageKey, message);
}

export function getMessage(callback: (err: Error | null, data: string | null) => void): void {
  client.get(messageKey, callback);
}
