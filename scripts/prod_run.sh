#!/bin/bash

##
## Run docker-compose with PRODUCTION configuration
##

SCRIPT_PATH=$(realpath "$BASH_SOURCE")
PARENT_DIR=$(dirname "$SCRIPT_PATH")
ROOT=$(dirname "$PARENT_DIR")

echo "Changing to project's root directory: $ROOT"
cd "$ROOT"

echo "Running docker-compose up --build in PRODUCTION mode"
docker-compose up --build
